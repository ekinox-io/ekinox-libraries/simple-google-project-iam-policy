variable "project_id" {
  type        = string
  description = "Project id use to define the google_project_iam_policy"
}

variable "users" {
  type        = map(list(string))
  description = "Mapping user email to list of roles"
  default     = {}
}

variable "groups" {
  type        = map(list(string))
  description = "Mapping user group email to list of roles"
  default     = {}
}

variable "service_accounts" {
  type        = map(list(string))
  description = "Mapping service account email to list of roles"
  default     = {}
}

variable "service_agents" {
  type        = list(object({ email : string, role : string }))
  description = "Mapping service account email to list of roles"
  default     = []
}
