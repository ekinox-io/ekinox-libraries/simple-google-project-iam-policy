# simple-google-project-iam-policy

A simpler way to authoritativly define the policy of a project.

```tf
// setup helping module
module "agents" {
  source = "git::https://gitlab.com/ekinox-io/ekinox-libraries/google-cloud-service-agents.git?ref=v1.0.4"

  project_number = local.project.number
}

// setup this module
module "simple_google_project_iam_policy" {
  source = "git::https://gitlab.com/ekinox-io/ekinox-libraries/simple-google-project-iam-policy.git?ref=v1.0.1"

  project_id = local.project.id

  users = {
    // any user email
    (local.users.you) : [
      // any roles
      "roles/datastore.user"
    ]
  }

  service_accounts = {
    // any service account email
    (google_service_account.my-account.email) : [
      // any roles
      "roles/bigquery.admin",
      "roles/datastore.user",
      "roles/storage.objectViewer"
    ]
  }

  service_agents = [
    // any service agents
    module.agents.artifact_registry,
    module.agents.google_cloud_functions,
    module.agents.google_cloud_run,
    module.agents.cloud_build,
    module.agents.cloud_build_builder
  ]
}
```

## License
MIT License

Copyright (c) 2022, Ekinox

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |


## Resources

| Name | Type |
|------|------|
| [google_project_iam_policy.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_policy) | resource |
| [google_iam_policy.this](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/iam_policy) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_groups"></a> [groups](#input\_groups) | Mapping user group email to list of roles | `map(list(string))` | `{}` | no |
| <a name="input_project_id"></a> [project\_id](#input\_project\_id) | Project id use to define the google\_project\_iam\_policy | `string` | n/a | yes |
| <a name="input_service_accounts"></a> [service\_accounts](#input\_service\_accounts) | Mapping service account email to list of roles | `map(list(string))` | `{}` | no |
| <a name="input_service_agents"></a> [service\_agents](#input\_service\_agents) | Mapping service account email to list of roles | `list(object({ email : string, role : string }))` | `[]` | no |
| <a name="input_users"></a> [users](#input\_users) | Mapping user email to list of roles | `map(list(string))` | `{}` | no |

## Outputs

No outputs.