locals {
  iam_bindings = transpose(merge(
    {for email, roles in var.users : "user:${email}" => roles},
    {for email, roles in var.groups : "group:${email}" => roles},
    {for email, roles in var.service_accounts : "serviceAccount:${email}" => roles}
  ))

  service_agent_bindings = {
    for agent in var.service_agents : agent.role => ["serviceAccount:${agent.email}"]
  }
}
