resource "google_project_iam_policy" "this" {
  project     = var.project_id
  policy_data = data.google_iam_policy.this.policy_data
}

data "google_iam_policy" "this" {
  audit_config {
    service = "allServices"

    audit_log_configs {
      log_type = "ADMIN_READ"
    }
    audit_log_configs {
      log_type = "DATA_READ"
    }
    audit_log_configs {
      log_type = "DATA_WRITE"
    }
  }

  dynamic "binding" {
    for_each = {for role, members in local.iam_bindings : role => sort(members) if length(members) != 0}
    content {
      role    = binding.key
      members = binding.value
    }
  }

  dynamic "binding" {
    for_each = local.service_agent_bindings
    content {
      role    = binding.key
      members = binding.value
    }
  }
}
